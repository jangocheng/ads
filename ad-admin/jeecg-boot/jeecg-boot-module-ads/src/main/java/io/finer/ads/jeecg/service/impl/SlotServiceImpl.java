package io.finer.ads.jeecg.service.impl;

import io.finer.ads.jeecg.entity.Slot;
import io.finer.ads.jeecg.mapper.SlotMapper;
import io.finer.ads.jeecg.service.ISlotService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 广告位
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
@Service
public class SlotServiceImpl extends ServiceImpl<SlotMapper, Slot> implements ISlotService {

}
