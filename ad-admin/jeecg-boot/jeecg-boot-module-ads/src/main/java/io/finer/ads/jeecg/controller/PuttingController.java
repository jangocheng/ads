package io.finer.ads.jeecg.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import io.finer.ads.jeecg.entity.Putting;
import io.finer.ads.jeecg.service.IPuttingService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 广告投放
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
@Api(tags="广告投放")
@RestController
@RequestMapping("/putting")
@Slf4j
public class PuttingController extends JeecgController<Putting, IPuttingService> {
	@Autowired
	private IPuttingService puttingService;

	/**
	 * 分页列表查询
	 *
	 * @param putting
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "广告投放-分页列表查询")
	@ApiOperation(value="广告投放-分页列表查询", notes="广告投放-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(Putting putting,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<Putting> queryWrapper = QueryGenerator.initQueryWrapper(putting, req.getParameterMap());
		Page<Putting> page = new Page<Putting>(pageNo, pageSize);
		IPage<Putting> pageList = puttingService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param putting
	 * @return
	 */
	@AutoLog(value = "广告投放-添加")
	@ApiOperation(value="广告投放-添加", notes="广告投放-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody Putting putting) {
		puttingService.save(putting);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param putting
	 * @return
	 */
	@AutoLog(value = "广告投放-编辑")
	@ApiOperation(value="广告投放-编辑", notes="广告投放-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody Putting putting) {
		puttingService.updateById(putting);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "广告投放-通过id删除")
	@ApiOperation(value="广告投放-通过id删除", notes="广告投放-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		puttingService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "广告投放-批量删除")
	@ApiOperation(value="广告投放-批量删除", notes="广告投放-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.puttingService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "广告投放-通过id查询")
	@ApiOperation(value="广告投放-通过id查询", notes="广告投放-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		Putting putting = puttingService.getById(id);
		if(putting==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(putting);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param putting
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Putting putting) {
        return super.exportXls(request, putting, Putting.class, "广告投放");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, Putting.class);
    }

}
