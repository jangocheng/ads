package io.finer.ads.jeecg.mapper;

import io.finer.ads.jeecg.entity.Channel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 广告频道
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
public interface ChannelMapper extends BaseMapper<Channel> {

}
