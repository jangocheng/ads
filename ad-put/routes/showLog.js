const Joi = require('joi');
const Controllers = require('../controllers');

let log = {
    method: 'post',
    path: '/show/log',
    config: {
        auth: false,
        description: 'Routing with parameters',
        notes: ' api',
        tags: ['api']
    },
    handler: Controllers.showLog.log
};


module.exports = log;